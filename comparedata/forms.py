from django import forms

class JobForm(forms.Form):
	"""
		Form to handle the creation of a job
	"""
	org_one = forms.CharField(required=True)
	org_two = forms.CharField(required=True)