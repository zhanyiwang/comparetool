# Generated by Django 2.2.6 on 2019-10-27 10:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('comparedata', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserToken',
            fields=[
                ('username', models.CharField(blank=True, max_length=255, primary_key=True, serialize=False)),
                ('instance_url', models.CharField(max_length=255)),
                ('org_id', models.CharField(max_length=255)),
                ('org_name', models.CharField(blank=True, max_length=255)),
                ('access_token', models.CharField(max_length=255)),
                ('refresh_token', models.CharField(max_length=255)),
            ],
        ),
    ]
