from django.urls import path

from . import views

urlpatterns = [
    # path('mylist/', views.MyList.as_view(), name='mylist'),
    # path('', views.index, name='index'),    
    path('', views.IndexView.as_view(), name='index'),
    # path('oauth-response', views.IndexView.as_view(), name='oauth_response'),
	path('oauth-response', views.OAuthResponse.as_view(), name='oauth_response'),
    path('loading/<str:job_id>/', views.LoadingPage.as_view(), name='loading_page'), 
    path('load-user-tokens', views.LoadUserTokens, name='load-user-tokens'),
    path('select-object/<str:job_id>/', views.SelectObject.as_view(), name='select_object'),
    path('job-status/<str:job_id>/', views.job_status, name='comparedata.views.job_status'),
    path('get-fields/<str:job_id>/<str:object_id>/', views.get_fields, name = 'comparedata.views.get_fields'),
    path('compare-data/<str:job_id>/<str:object_id>/', views.execute_data_compare, name ='comparedata.views.execute_data_compare'),
    path('compare-data-result/<str:job_id>/', views.CompareDataResult.as_view(), name='compare_data_result'),
    path('unmatched-rows/<str:job_id>/<str:org_no>/', views.get_unmatched_rows, name='comparedata.views.get_unmatched_rows'),
]

