import os
from celery import Celery

# Celery config
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sfdatacompare.settings')
#app = Celery('tasks', broker=os.environ.get('REDIS_URL', 'redis://localhost'))
app = Celery('tasks', broker='redis://localhost:6379/0')

app.config_from_object('django.conf:settings')
app.autodiscover_tasks()