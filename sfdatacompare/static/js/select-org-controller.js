/*
	Angular controller for all the Compare Objects results page
*/

var selectOrgApp = angular.module("selectOrgApp", ['ngRoute', 'ngResource']);

selectOrgApp.controller("SelectOrgController", function ($scope, $http) {
    $scope.init = function () {
        $scope.test = 'hello';
        $http(
            {
                method: 'GET',
                url: '/load-user-tokens',
            }).
            success(function (datas, status) {

                var tokens = []
                for (let index = 0; index < datas.length; index++) {
                    element = datas[index];
                    token=element.fields;
                    token.username=element.pk;
                    tokens.push(token);

                }
                $scope.all_tokens = tokens;
            });
    }
    $scope.userChange = function () {
        if(userName1 !== 'Select Org' && userName2 !== 'Select Org'){
            $('#compareOrgs').show();
        } 
    }

});
